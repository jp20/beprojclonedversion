from django.conf.urls import url
from django.contrib import admin
from .views import chat_view, index_view,line_chart,line_chart_json # webhook#, add_book
from django.views.generic import TemplateView

urlpatterns = [
    url(r'chat/$', chat_view, name='chat'),
    url(r'^$', index_view, name='index'),
    # url(r'^chart/', add_book, name='chart' ),
    url(r'^admin/', admin.site.urls),
    #url(r'^webhook/$', webhook, name='webhook'),

    url(r'^line_chart/$', line_chart,name='line_chart'),
    url(r'^line_chart/json/$', line_chart_json,name='line_chart_json')
]
