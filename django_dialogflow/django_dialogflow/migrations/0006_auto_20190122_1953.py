# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2019-01-22 19:53
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django_dialogflow', '0005_templog_humidity'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='templog',
            table='templog',
        ),
    ]
